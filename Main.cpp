#include <iostream>
using namespace std;

class Stack
{
private:
	int size = 1;
	int i = 0;
	int* array = 0;
public:
	Stack()
	{
		array = new int[size];
	}

	~Stack()
	{
		delete[] array;
		array = 0;
	}

	void write()
	{
		cout << "Current contents of stack" << "\n";
		for (int k = 0; k < i; k++)
		{
				cout << array[k] << " ";
		}
		cout << "\n";
		cout << "\n";
	}

	void pop()
	{
		if (i == 0)
		{
			cout << "Nothing to delete!" << "\n";
			cout << "\n";
		}
		else
		{
			i--;
			write();
		}
	}

	void push(int new_element)
	{
		if (size == i)
		{
			int* tmp_array = new int[size * 2];
			for (size_t j = 0; j < size; j++)
			{
				tmp_array[j] = array[j];
			}
			delete[] array;
			array = tmp_array;
			size *= 2;
			array[i++] = new_element;
			write();
		}
		else
		{
			array[i++] = new_element;
			write();
		}
	}
};

int main()
{
	int push_element;
	int choice;
	cout << "\n";
	Stack MyStack;
	while (2 > 1)
	{
		cout << "Enter 1 to add new element, 0 to delete the last one or something else to exit ";
		cin >> choice;
		cout << "\n";
		if (choice == 1)
		{
			cout << "Enter the new element ";
			cin >> push_element;
			cout << "\n";
			MyStack.push(push_element);
		}
		else if (choice == 0)
		{
			MyStack.pop();
		}
		else
		{
			return 0;
		}
	}
}